# Gtk on screen keyboard (gtk-keyboard-osk)

Screen keyboard written gtk3 and pynput

## Advantages:

* Small size
* Minimal dependencies
* Installation not needed (portable)
* Widget support

## Usage:
`cd src && python3 application.py`

# Requirements:

* gir1.2-gtk-3.0 (python3-gobject)
* python3-pynput (you can install with pip3)
* console-setup (for keyboard layout)

# Installation:
```
git clone https://gitlab.com/sulincix/gtk-keyboard-osk.git gtk-keyboard-osk
cd gtk-keyboard-osk
make install
```
